// Mock out direct DOM access.
// See https://github.com/facebook/jest/issues/2297#issuecomment-266835690
Object.defineProperty(document, 'getElementById', {
  value: () => {
    const list1 = document.createElement('a')
    list1.setAttribute('id', 'list-1')
    list1.scrollIntoView = () => {}
    return list1
  },
})
