import * as R from 'ramda'
import { getData } from '../api/getData'
import parseData from '../util/parseData'
import search from '../util/search'
import { allSkillLevelKeys } from '../util/skillLevels'
import * as types from './mutation_types'

export const loadDataStart = ({ commit }) => commit(types.LOAD_DATA_START)
export const loadDataSuccess = ({ commit }, data) =>
  commit(types.LOAD_DATA_SUCCESS, data)
export const loadDataFailure = ({ commit }, data) =>
  commit(types.LOAD_DATA_FAILURE, data)

export const loadDataFlow = ({ dispatch }) => {
  dispatch('loadDataStart')

  getData()
    .then((response) => {
      const parsedData = parseData(response.data)
      dispatch('loadDataSuccess', parsedData)
    })
    .catch((error) => {
      const errorResponseData = error.response ? error.response.data : ''
      dispatch(
        'loadDataFailure',
        `${error} ${JSON.stringify(errorResponseData)}`
      )
    })
}

export const resetToDefaultsFlow = ({ commit, getters }, data) => {
  commit(types.UPDATE_SELECTED_SKILL_LEVELS, allSkillLevelKeys)
  commit(types.UPDATE_SEARCH_TEXT, '')
  commit(types.RESET_LISTS_SORT_BY, { getters })
  commit(types.RESET_LISTS_SHOW_COLLAPSE, { getters, data })
}

export const updateSearchText = ({ commit }, data) =>
  commit(types.UPDATE_SEARCH_TEXT, data)

export const updateListSortBy = ({ commit }, data) =>
  commit(types.UPDATE_LIST_SORT_BY, data)

export const updateListShowCollapse = ({ commit }, data) =>
  commit(types.UPDATE_LIST_SHOW_COLLAPSE, data)

export const filterItemsSuccess = ({ commit }, data) =>
  commit(types.FILTER_ITEMS_SUCCESS, data)

export const filterItemsFlow = ({ state, dispatch, _ }) => {
  if (state.searchText !== '') {
    const filteredItemIds = search(
      ['name', 'searchableDesc'],
      R.map((itemId) => state.items.byId[itemId])(state.items.allIds),
      state.searchText
    )
    dispatch('filterItemsSuccess', filteredItemIds)
  }
}
