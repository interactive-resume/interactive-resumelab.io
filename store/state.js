import { allSkillLevelKeys } from '../util/skillLevels'
import defaultNormalizedStructure from './defaultNormalizedStateStructure'

export default () => ({
  aboutTitle: '',
  htmlAboutContent: '',
  headerName: '',
  headerContact: '',
  lists: defaultNormalizedStructure(),
  items: defaultNormalizedStructure(),
  searchText: '',
  selectedSkillLevels: allSkillLevelKeys,
  filteredItemIds: [],
  error: '',
  isLoading: false,
})
