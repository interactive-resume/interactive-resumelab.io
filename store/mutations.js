import * as R from 'ramda'
import transformDataToState from '../util/transformData'
import * as types from './mutation_types'

export default {
  [types.LOAD_DATA_START](state) {
    state.isLoading = true
  },
  [types.LOAD_DATA_SUCCESS](state, data) {
    transformDataToState(data, state)
  },
  [types.LOAD_DATA_FAILURE](state, data) {
    state.error = data
    state.isLoading = false
  },
  [types.UPDATE_SEARCH_TEXT](state, data) {
    state.searchText = data
  },
  [types.UPDATE_LIST_SORT_BY](state, data) {
    const list = state.lists.byId[data.listId]
    list.sortBy = data.sortBy
  },
  [types.UPDATE_LIST_SHOW_COLLAPSE](state, data) {
    const list = state.lists.byId[data.listId]
    list.showCollapse = data.showCollapse
  },
  [types.RESET_LISTS_SORT_BY](state, { getters }) {
    // NOTE: Using getters in mutations is not officially supported, but you
    // can pass them from the action as a hack.  See:
    // https://github.com/vuejs/vuex/issues/684
    // https://stackoverflow.com/a/43259220/25192
    R.forEachObjIndexed((list) => (list.sortBy = 'default'), getters.lists)
  },
  [types.RESET_LISTS_SHOW_COLLAPSE](state, { getters, data }) {
    const lists = getters.lists
    R.forEachObjIndexed((list) => {
      if (list.showCollapse === false) {
        list.showCollapse = true
        // NOTE: We are using the   data payload to pass the a handle on the
        // vue instance from the component, so we can emit the root event
        // Note that `this._vm` will NOT work - that's a different vue instance.
        // TODO: This seems hacky, but I don't know how to properly emit
        // the event from a vuex action outside the collapse-able component
        // otherwise.  References:
        // https://bootstrap-vue.js.org/docs/components/collapse/
        // https://github.com/vuejs/vuex/issues/1399#issuecomment-425331925
        data.root.$emit('bv::toggle::collapse', `collapse-list-${list.id}`)
      }
    }, lists)
  },
  [types.UPDATE_SELECTED_SKILL_LEVELS](state, data) {
    state.selectedSkillLevels = data
  },
  [types.FILTER_ITEMS_SUCCESS](state, data) {
    state.filteredItemIds = data
  },
}
