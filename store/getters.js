import * as R from 'ramda'

export default {
  lists(state) {
    return R.map((id) => state.lists.byId[id], state.lists.allIds)
  },
}
