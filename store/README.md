# Notes on denormalized store structure

```javascript
const store = {
  lists: {
    byId: {
      1: {
        id: 1, // denormalize id into list (for sort dropdown id)
        name: 'Skills',
        itemIds: [1],
        sortBy: 'default',
        showCollapse: true,
      },
      2: {
        id: 2, // denormalize id into list (for sort dropdown id)
        name: 'Jobs',
        itemIds: [2],
        sortBy: 'alphabetical_asc',
        showCollapse: true,
      },
    },
    allIds: [1,2]
  },
  items: {
    byId: {
      1: {
        id: 1, // denormalize id into item for more efficient searching
        level: played,
        start: '2001-07-02',
        end: '2001-08-02',
        name: 'skill 1',
        markdownDesc: 'Skill **One**',
        htmlDesc: 'Skill <strong>One</strong>',
        searchableDesc: 'Skill One',
      },
      2: {
        id: 2, // denormalize id into item for more efficient searching
        name: 'job 2',
        rawDesc: 'Job **One**',
        markedDesc: 'Job <strong>One</strong>',
        searchableDesc: 'Job One',
      },
    },
    allIds: [1,2]
  },
  filteredItemIds: [2],
  /* ...other scalar top-level state fields... */
}
console.log(store)
```

# Notes onAction Naming

## For Multi-Step Flows
* For multi-step-flows, actions, mutations and mutation types are named with
  one or more of the following: `*_START`, `*_SUCCESS`, `*_FAILURE`
* Action functions to group actions/mutations are named: `*Flow`
* For example, see `LOAD_DATA_*` and `loadDataFlow`

## For Single Step Flows

* No suffix (e.g `_START`) is needed.

# Vue Generated App Boilerplate

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains your Vuex Store files.
Vuex Store option is implemented in the Nuxt.js framework.

Creating a file in this directory automatically activates the option in the framework.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/vuex-store).
