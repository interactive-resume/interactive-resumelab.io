import search from '../../util/search'

describe('search', () => {
  let keys
  let items

  beforeEach(() => {
    keys = ['key1', 'key2']
    items = [
      { id: 1, key1: 'aa', key2: 'one cc' },
      { id: 2, key1: 'ab', key2: 'two' },
      { id: 3, key1: 'cc', key2: 'three' },
    ]
  })

  test('single id match works', () => {
    const result = search(keys, items, 'aa')
    expect(result).toEqual([1])
  })

  test('multiple id match works', () => {
    const result = search(keys, items, 'a')
    expect(result).toEqual([1, 2])
  })

  test('multiple key match works', () => {
    const result = search(keys, items, 'cc')
    expect(result).toEqual([1, 3])
  })
})
