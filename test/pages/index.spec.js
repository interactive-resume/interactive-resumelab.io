import { readFileSync } from 'fs'
import { createLocalVue, mount } from '@vue/test-utils'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'
// eslint-disable-next-line
import BootstrapVue from 'bootstrap-vue'
import flushPromises from 'flush-promises'
import index from '~/pages/index'

const mockAxios = new MockAdapter(axios)

const localVue = createLocalVue()
localVue.use(BootstrapVue)

const yamlData = readFileSync(
  'static/interactive-resume-data-dev-and-test-fixture.yml',
  'utf8'
)

describe('index', () => {
  beforeAll(() => {
    process.env = Object.assign(process.env, { BOOTSTRAP_VUE_NO_WARN: 'true' })
  })

  describe('happy path', () => {
    let wrapper

    beforeEach(async () => {
      mockAxios.onGet().reply(200, yamlData)
      wrapper = mount(index, { localVue })
      await flushPromises()
    })

    test('data loading and rendering', () => {
      // test header rendering
      expect(wrapper.text()).toMatch('Napoleon')

      // test list name
      expect(wrapper.text()).toMatch('jobs')

      // test item name
      expect(wrapper.text()).toMatch("friggin' crossbow")

      // test item description
      expect(wrapper.text()).toMatch('hunting skills')
    })

    test('date formatting', () => {
      // test date formatting
      expect(wrapper.html()).toMatch('7.2001&nbsp;-') // nonbreaking space
      expect(wrapper.text()).toMatch(/7.2001\s-\s8.2001/) // start and end
      expect(wrapper.text()).toMatch(/1.2000\s-\scurrent/) // start, no end
      expect(wrapper.text()).toMatch(/until\s11.2004/) // end, no start
      expect(wrapper.text()).toMatch(/hunter\s+toolbox\s+current\s+Hunted/) // no start or end
    })

    test('markdown', () => {
      // test markdown
      expect(wrapper.html()).toMatch(
        '<strong>Nunchuck</strong> sk<strong>illz</strong>'
      )
    })

    test('searching', () => {
      // test search filtering
      const searchTextInput = wrapper.find('#search-text')
      searchTextInput.setValue('bow')
      expect(wrapper.text()).toMatch('Bow hunting')
      expect(wrapper.text()).not.toMatch('Nunchuck')

      // test that searching excludes markdown and html tags
      searchTextInput.setValue('skillz')
      expect(wrapper.html()).toMatch('sk<strong>illz</strong>')

      // test display everything again if filter is cleared
      searchTextInput.setValue('')
      expect(wrapper.text()).toMatch('Nunchuck')
    })

    test('skill level filtering', () => {
      // test search filtering
      const playedCheckbox = wrapper.find('input[type=checkbox][value=played]')
      expect(wrapper.text()).toMatch('nunchuck')
      playedCheckbox.setChecked(false)
      expect(wrapper.text()).not.toMatch('nunchuck')
      expect(wrapper.text()).toMatch(/crossbow\s+once/)

      // test close button
      const closeButton = wrapper.find('#navSkillLevelCheckboxesClose')
      closeButton.trigger('click')
      // TODO: the above gets code coverage, but how to assert display: none?
      // Vue test utils provides .exists and .isVisible, but neither of those
      // are intended (or can be used) to test display: none which is what
      // the bootstrap-vue dropdown uses...
    })

    test('sorting', () => {
      const skillsSortSelect = wrapper.find('#sort-select-1')
      const jobsSortSelect = wrapper.find('#sort-select-2')

      // by default order
      expect(wrapper.text()).toMatch(/nunchuck.*bow.*computer/gms)

      // alphabetically asc
      skillsSortSelect.setValue('alphabetical_asc')
      expect(wrapper.text()).toMatch(/bow.*computer.*nunchuck/gms)

      // fall back to default order on invalid sort
      skillsSortSelect.setValue('invalid')
      expect(wrapper.text()).toMatch(/nunchuck.*bow.*computer/gms)

      // alphabetically desc
      skillsSortSelect.setValue('alphabetical_desc')
      expect(wrapper.text()).toMatch(/nunchuck.*computer.*bow/gms)

      // start date asc
      skillsSortSelect.setValue('start_asc')
      expect(wrapper.text()).toMatch(
        /computer.*until.*12.2002.*bow.*1.2000.*nunchuck.*7.2001/gms
      )

      // start date desc
      skillsSortSelect.setValue('start_desc')
      expect(wrapper.text()).toMatch(
        /nunchuck.*7.2001.*bow.*1.2000.*computer.*until.*12.2002/gms
      )

      // end date asc
      skillsSortSelect.setValue('end_asc')
      expect(wrapper.text()).toMatch(
        /nunchuck.*8.2001.*computer.*12.2002.*bow.*current/gms
      )

      // end date desc
      skillsSortSelect.setValue('end_desc')
      expect(wrapper.text()).toMatch(
        /bow.*current.*computer.*12.2002.*nunchuck.*8.2001/gms
      )

      // fallback to name sort if dates are missing
      jobsSortSelect.setValue('start_asc')
      expect(wrapper.text()).toMatch(/secret.*wolverine/gms)
      jobsSortSelect.setValue('start_desc')
      expect(wrapper.text()).toMatch(/wolverine.*secret/gms)
      jobsSortSelect.setValue('end_asc')
      expect(wrapper.text()).toMatch(/secret.*wolverine/gms)
      jobsSortSelect.setValue('end_desc')
      expect(wrapper.text()).toMatch(/wolverine.*secret/gms)

      // can set back to default
      skillsSortSelect.setValue('default')
      expect(wrapper.text()).toMatch(/nunchuck.*bow.*computer/gms)
    })

    test('reset', () => {
      window.scrollTo = () => {} // mock out scrollTo; it's not supported
      const resetLink = wrapper.find('#reset-to-defaults > a')

      // enter search text
      const searchTextInput = wrapper.find('#search-text')
      searchTextInput.setValue('bow')
      expect(wrapper.text()).not.toMatch('Nunchuck')

      // test reset for search text
      resetLink.trigger('click')
      expect(wrapper.text()).toMatch('Nunchuck')

      // uncheck skill checkbox
      const playedCheckbox = wrapper.find('input[type=checkbox][value=played]')
      playedCheckbox.setChecked(false)
      expect(wrapper.text()).not.toMatch('nunchuck')

      // test reset for skill checkbox
      resetLink.trigger('click')
      expect(wrapper.text()).toMatch('Nunchuck')

      // sort a list
      const skillsSortSelect = wrapper.find('#sort-select-1')
      skillsSortSelect.setValue('alphabetical_asc')
      expect(wrapper.text()).toMatch(/bow.*computer.*nunchuck/gms)

      // test reset for list sorting
      resetLink.trigger('click')
      expect(wrapper.text()).toMatch(/nunchuck.*bow.*computer/gms)

      // collapse a list
      const listToggle = wrapper.find('#list-toggle-1')
      listToggle.trigger('click')

      // test reset for list collapse
      resetLink.trigger('click')
    })
  })

  describe('error path', () => {
    test('with response', async () => {
      mockAxios.onGet().reply(500, { error: 'errortext' })

      const wrapper = mount(index, { localVue })
      await flushPromises()
      expect(wrapper.text()).toMatch(/error.*errortext/)
    })

    test('without response', async () => {
      mockAxios.onGet().reply(200, null)

      const wrapper = mount(index, { localVue })
      await flushPromises()
      expect(wrapper.text()).toMatch(/TypeError.*null/)
    })
  })
})
