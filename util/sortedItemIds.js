import * as R from 'ramda'

export default function sortedItemIds(sortBy, allItems, itemIdsToSort) {
  // TODO: lots of opportunities to DRY this up with currying, higher order functions, etc
  // See https://gitlab.com/interactive-resume/interactive-resume.gitlab.io/issues/43

  if (sortBy === 'default') {
    return itemIdsToSort
  }

  const byId = allItems.byId

  const nameSortAsc = (itemIdA, itemIdB) =>
    byId[itemIdA].name.localeCompare(byId[itemIdB].name)
  const nameSortDesc = (itemIdA, itemIdB) =>
    byId[itemIdB].name.localeCompare(byId[itemIdA].name)

  const startSortAsc = (itemIdA, itemIdB) => {
    const diff = byId[itemIdA].startMillis - byId[itemIdB].startMillis
    if (diff === 0) {
      return nameSortAsc(itemIdA, itemIdB)
    }
    return diff
  }

  const startSortDesc = (itemIdA, itemIdB) => {
    const diff = byId[itemIdB].startMillis - byId[itemIdA].startMillis
    if (diff === 0) {
      return nameSortDesc(itemIdA, itemIdB)
    }
    return diff
  }

  const boost = 2000000000000
  const endSortAsc = (itemIdA, itemIdB) => {
    // give missing end dates higher priority (they are assumed to be current)
    let aMillis = byId[itemIdA].endMillis
    let bMillis = byId[itemIdB].endMillis
    if (aMillis === 0) {
      aMillis += boost
    }
    if (bMillis === 0) {
      bMillis += boost
    }
    const diff = aMillis - bMillis
    if (diff === 0) {
      return nameSortAsc(itemIdA, itemIdB)
    }
    return diff
  }

  const endSortDesc = (itemIdA, itemIdB) => {
    // give missing end dates higher priority (they are assumed to be current)
    let aMillis = byId[itemIdA].endMillis
    let bMillis = byId[itemIdB].endMillis
    if (aMillis === 0) {
      aMillis += boost
    }
    if (bMillis === 0) {
      bMillis += boost
    }
    const diff = bMillis - aMillis
    if (diff === 0) {
      return nameSortDesc(itemIdA, itemIdB)
    }
    return diff
  }

  let sortAlgorithm
  switch (sortBy) {
    case 'alphabetical_asc':
      sortAlgorithm = nameSortAsc
      break
    case 'alphabetical_desc':
      sortAlgorithm = nameSortDesc
      break
    case 'start_asc':
      sortAlgorithm = startSortAsc
      break
    case 'start_desc':
      sortAlgorithm = startSortDesc
      break
    case 'end_asc':
      sortAlgorithm = endSortAsc
      break
    case 'end_desc':
      sortAlgorithm = endSortDesc
      break
    // NOTE: there is no default case - because of vue caching, it would never
    // be executed and thus is not needed - tests and code coverage proves this.
  }

  const sortedItemIds = R.sort(sortAlgorithm, itemIdsToSort)
  return sortedItemIds
}
