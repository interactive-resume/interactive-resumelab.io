import * as R from 'ramda'

export const allSkillLevels = {
  played: 'I have played around with it for fun',
  once: 'I have used it on a single job or project',
  often: 'I have used it on multiple jobs or projects',
  toolbox: "It's part of my toolbox; I use it daily",
  teach: 'I know it in depth, I could teach a workshop or class on it',
  unspecified: 'Unspecified',
}

export const skillLevelOptions = () => {
  const opts = []
  R.map((k) => opts.push({ text: k, value: k }), allSkillLevelKeys)
  return opts
}

export const allSkillLevelKeys = R.keys(allSkillLevels)

export const isValidSkillLevel = (level) => {
  return R.includes(level, R.keys(allSkillLevels))
}
