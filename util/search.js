import Fuse from 'fuse.js'
import * as R from 'ramda'

export default function search(keys, items, pattern) {
  // NOTE: This function assumes all IDs are globally unique

  const options = {
    id: 'id',
    keys,
    threshold: 0.1,
    tokenize: true,
    location: 1,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
  }

  // fuse.js doesn't like the raw observer-enhanced state objects for searching,
  // so convert them into plain objects.
  const allProps = R.concat([options.id], keys)
  const toPlainObject = (item) => R.pick(allProps, item)
  const plainObjectItems = R.map(toPlainObject, items)

  const fuse = new Fuse(plainObjectItems, options)
  const stringIds = fuse.search(pattern)
  const ids = R.map((stringId) => parseInt(stringId))(stringIds)
  return R.sort((a, b) => a - b)(ids)
}
