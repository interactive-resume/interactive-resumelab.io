import marked from 'marked'
import * as R from 'ramda'
import defaultNormalizedStructure from '../store/defaultNormalizedStateStructure'

export default function transformDataToState(data, state) {
  // PROCESS NON-LIST DATA
  state.aboutTitle = data.about.title
  state.htmlAboutContent = marked(data.about.content)

  state.headerName = data.header.name
  state.headerContact = data.header.contact

  // PROCESS LISTS DATA
  // https://github.com/ramda/ramda/issues/2091
  const forEachObjIndexed = R.addIndex(R.forEachObjIndexed)

  // exclude non-list entries in data
  const onlyListData = R.pickBy((_, k) => k !== 'about' && k !== 'header', data)

  // declare temp objects to build up state
  const lists = defaultNormalizedStructure()
  const items = defaultNormalizedStructure()

  // iterate over the lists
  let itemId = 0
  forEachObjIndexed((list, listName, _, idx) => {
    const listId = idx + 1
    const itemIdsForThisList = []

    // iterate over the items in this list
    R.forEach((item) => {
      itemId = itemId + 1
      const [startMillis, start, endMillis, end] = parseDates(
        item.start,
        item.end
      )
      const htmlDesc = marked(item.desc)
      const searchableDesc = stripHtml(htmlDesc)
      items.byId[itemId] = {
        id: itemId,
        name: item.name,
        level: item.level,
        start,
        startMillis,
        end,
        endMillis,
        markdownDesc: item.desc, // raw unformatted markdown
        htmlDesc, // formatted markdown
        searchableDesc, // stripped of any markdown or html tags
      }
      itemIdsForThisList.push(itemId)
      items.allIds.push(itemId)
    })(list)

    // store this list into lists
    lists.byId[listId] = {
      id: listId,
      name: listName,
      itemIds: itemIdsForThisList,
      sortBy: 'default',
      showCollapse: true,
    }
    lists.allIds.push(listId)
  })(onlyListData)

  // NOTE 1: arrays in state cannot be updated directly, or vuex may not detect changes
  // See: https://vuejs.org/v2/guide/reactivity.html#Change-Detection-Caveats
  // NOTE 2: the normalized state structure tree of data "tables" MUST be populated
  //         from the "bottom" or "leaves" up.  Otherwise there will be references
  //         to items which do not yet exist
  state.items = items
  state.lists = lists
  state.isLoading = false
}

function parseDates(startDate, endDate) {
  if (!startDate && !endDate) {
    return [0, '', 0, 'current']
  }
  const [startMillis, start] = parseDate(startDate, 'start')
  const [endMillis, end] = parseDate(endDate, 'end')
  return [startMillis, start, endMillis, end]
}

function parseDate(dateFromData, type) {
  // NOTE: "marked" markdown parser automatically parses dates, so we get a Date object
  //       This is parsed in UTC, which means that bare dates such as `2001-01-01`
  if (!dateFromData) {
    const formatted = type === 'start' ? 'until' : 'current'
    return [0, formatted]
  }
  const date = R.clone(dateFromData) // keep this function pure; don't modify args
  const millis = date.getTime()
  let formatted = `${date.getMonth() + 1}.${date.getFullYear()}`
  if (type === 'start') {
    formatted = `${formatted}&nbsp;-`
  }
  return [millis, formatted]
}

function stripHtml(html) {
  const element = document.createElement('div')
  element.innerHTML = html
  return element.textContent
}
