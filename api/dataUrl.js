export function dataUrl() {
  switch (process.env.NODE_ENV) {
    case 'production':
      return 'https://interactive-resume-data-chad-woolley.gitlab.io/interactive-resume-data-chad-woolley.yml'
    case 'development':
      return 'http://localhost:3000/interactive-resume-data-performance-testing-fixture.yml'
    // Uncomment to use actual data file for local testing and exploration
    // return 'http://localhost:3000/interactive-resume-data-chad-woolley.yml'
    case 'test':
      return 'mocked anyway'
    default:
      throw new Error(`unexpected NODE_ENV: ${process.env.NODE_ENV}`)
  }
}
